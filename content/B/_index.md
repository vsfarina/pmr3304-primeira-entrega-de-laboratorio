---
title: "Pensadores Posteriores"
date: 2021-09-18T23:28:40-03:00
draft: false
---
Como o grande pensador que foi, Aristóteles influencia as ideias até hoje. Em parte, isso é refletido pelos grandes nomes que nele se basearam para suas obras, como:

1. Tomás de Aquino: Um dos maiores filósofos e teólogos da Idade Média, Tomás de Aquino integrou a filosofia aristotélica com a teologia cristã. Ele utilizou os conceitos de Aristóteles sobre substância, essência e causa para desenvolver sua própria filosofia e teologia, especialmente na obra "Suma Teológica".

2. René Descartes: Conhecido como o pai da filosofia moderna, Descartes foi fortemente influenciado pela lógica e pelo método científico de Aristóteles. Ele adaptou e criticou algumas ideias aristotélicas, mas a influência de Aristóteles é evidente em sua abordagem sistemática e analítica.

3. Galileu Galilei: Considerado o pai da ciência moderna, Galileu foi influenciado pela abordagem empírica de Aristóteles. Embora ele tenha desafiado muitas das ideias aristotélicas sobre física, a metodologia de observação e experimentação de Aristóteles foi fundamental para o desenvolvimento do método científico de Galileu.
