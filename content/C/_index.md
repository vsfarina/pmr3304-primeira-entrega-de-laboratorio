---
title: "Pessoal"
date: 2021-09-18T23:28:40-03:00
draft: false
---
Aristóteles nasceu em Estagira, uma pequena cidade na Macedônia, em 384 a.C. Filho de Nicômaco, médico do rei Amintas III da Macedônia, Aristóteles cresceu em um ambiente que valorizava o conhecimento e a ciência. Com a morte prematura de seus pais, foi enviado para Atenas aos 17 anos para estudar na Academia de Platão, onde permaneceu por cerca de 20 anos. Embora suas ideias divergentes eventualmente o afastassem do platonismo, foi durante esse período que começou a moldar suas próprias teorias filosóficas.

Aristóteles foi casado duas vezes. Sua primeira esposa foi Pítias, com quem teve uma filha, também chamada Pítias. Após a morte de sua primeira esposa, Aristóteles casou-se com Herpílides, com quem teve um filho chamado Nicômaco. Esses relacionamentos pessoais profundamente influenciaram suas reflexões sobre ética e a vida em sociedade.

Sua relação com Alexandre, o Grande, também é notável. Quando Alexandre era jovem, Aristóteles foi convidado por Filipe II da Macedônia para ser tutor do futuro conquistador. Essa posição influenciou não só a vida de Alexandre, mas também a maneira como Aristóteles pensava sobre política e liderança.

Após o fim do seu relacionamento com Alexandre, Aristóteles fundou sua própria escola, o Liceu, em Atenas. Era um local vibrante, onde ele caminhava e discutia suas ideias com seus alunos. Este período foi um dos mais produtivos de sua vida, mas também lhe trouxe inimigos políticos. Acusado de impiedade, Aristóteles deixou Atenas para evitar o mesmo destino de Sócrates. Ele se retirou para Cálcis, na ilha de Eubeia, onde morreu em 322 a.C.